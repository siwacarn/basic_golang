package main

import "fmt"

type Person struct {
	FirstName string
	LastName  string
}

func main() {
	// mark := Person{FirstName: "Mark", LastName: "Nomoon"}
	// fmt.Println(mark)

	var mark Person

	mark.FirstName = "Mark"
	mark.LastName = "Nomoon"

	fmt.Println(mark)
	// %v print value in struct
	// %+v print vlaue and field struct
	fmt.Printf("%+v", mark)
}
